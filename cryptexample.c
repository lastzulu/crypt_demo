#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <assert.h>

#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/ec.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>
#include <openssl/kdf.h>

typedef unsigned char byte;
typedef unsigned int  uint;

#define KEY_SIZE    32 // 256 bits
#define IV_SIZE     16 // 128 bits CBC block size
#define GCM_TAG_LEN 16
#define MAX_BUFF    4096

int handleErrors( void )
{
    fprintf( stderr, "Failure Occurred\n" );
    ERR_print_errors_fp( stderr );
    fflush( stderr );
    return -1;
}

byte* genECDHSharedSecret( EVP_PKEY *pri, EVP_PKEY *pub, size_t *secretLen )
{
    EVP_PKEY_CTX   *ctx          = NULL;
    unsigned char  *sharedSecret = NULL;

    if ( ( NULL == ( ctx = EVP_PKEY_CTX_new( pri, NULL ) ) ) || // set private
         ( 1 != EVP_PKEY_derive_init( ctx ) ) ||
         ( 1 != EVP_PKEY_derive_set_peer( ctx, pub ) ) || // set peer public
         ( 1 != EVP_PKEY_derive( ctx, NULL, secretLen ) ) ||
         ( NULL == ( sharedSecret = OPENSSL_malloc( *secretLen ) ) ) ||
         ( 1 != EVP_PKEY_derive( ctx, sharedSecret, secretLen ) ) )
    {
        ERR_print_errors_fp( stderr );
        return NULL;
    }
    else
    {
        EVP_PKEY_CTX_free( ctx );
        return sharedSecret;
    }
}

int genECDHKey( EVP_PKEY **key, unsigned int curveName )
{
    EVP_PKEY_CTX   *paramCtx = NULL;
    EVP_PKEY_CTX   *keyCtx   = NULL;
    EVP_PKEY       *ecparams = NULL;
    int            status    = 1;

    if ( ( NULL == ( paramCtx = EVP_PKEY_CTX_new_id( EVP_PKEY_EC, NULL ) ) ) ||
         ( 1 != EVP_PKEY_paramgen_init( paramCtx ) ) ||
         ( 1 != EVP_PKEY_CTX_set_ec_paramgen_curve_nid( paramCtx, curveName ) ) ||
         ( 1 != EVP_PKEY_paramgen( paramCtx, &ecparams ) ) ||
         ( NULL == ( keyCtx = EVP_PKEY_CTX_new( ecparams, NULL ) ) ) ||
         ( 1 != EVP_PKEY_keygen_init( keyCtx ) ) ||
         ( 1 != EVP_PKEY_keygen( keyCtx, key ) ) )
    {
        ERR_print_errors_fp( stderr );
        status = 0; //failure
    }

    EVP_PKEY_CTX_free( paramCtx );
    EVP_PKEY_CTX_free( keyCtx );
    EVP_PKEY_free( ecparams );

    return status;
}

int genSessionKey( byte *sessionKey, byte *sharedSecret, uint sharedSecretLen,
                   byte *nonce, uint *counter, uint size )
{
    EVP_PKEY_CTX *pctx    = NULL;
    size_t        keysize = size;

    if ( ( NULL == ( pctx = EVP_PKEY_CTX_new_id( EVP_PKEY_HKDF, NULL ) ) ) ||
         ( EVP_PKEY_derive_init(pctx) <= 0 ) ||
         ( EVP_PKEY_CTX_set_hkdf_md( pctx, EVP_sha256() ) <= 0 ) ||
         ( EVP_PKEY_CTX_set1_hkdf_salt( pctx, nonce, size ) <= 0 ) ||
         ( EVP_PKEY_CTX_set1_hkdf_key( pctx, sharedSecret, sharedSecretLen ) <= 0 ) ||
         ( EVP_PKEY_CTX_add1_hkdf_info( pctx, counter, sizeof( uint ) ) <= 0 ) ||
         ( EVP_PKEY_derive( pctx, sessionKey, &keysize ) <= 0 ) )
    {
        ERR_print_errors_fp( stderr );
        return 0; //failure
    }
    else
    {
        EVP_PKEY_CTX_free( pctx );
        return 1; // success
    }
}

//Source: https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
int encryptCBC( byte *plaintext, int plaintext_len, byte *key,
                byte *iv, byte *ciphertext )
{
    EVP_CIPHER_CTX *ctx;
    int len;
    int ciphertext_len;
    int status = 0;

    /* Create and initialise the context */
    if( ( !( ctx = EVP_CIPHER_CTX_new() ) ) ||
      ( 1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv ) ) )
    {
        status = handleErrors();
    }

    /* Encrypt the plaintext */
    if( 1 != EVP_EncryptUpdate( ctx, ciphertext, &len, plaintext, plaintext_len ) )
    {
        status = handleErrors();
    }

    ciphertext_len = len;

    if( 1 != EVP_EncryptFinal_ex( ctx, ciphertext + len, &len ) )
    {
        status = handleErrors();
    }

    ciphertext_len += len;

    EVP_CIPHER_CTX_free( ctx );

    return ( 0 == status ) ? ciphertext_len : status;
}

//Source: https://github.com/openssl/openssl/blob/master/demos/evp/aesgcm.c
int encryptGCM( byte *plaintext, int plaintext_len, byte *key,
                byte *iv, byte *ciphertext, byte *tag )
{
    EVP_CIPHER_CTX *ctx;
    int len;
    int ciphertext_len;
    int status = 0;
 
    /* Create and initialise the context */
    if( ( !( ctx = EVP_CIPHER_CTX_new() ) ) ||
      ( 1 != EVP_EncryptInit_ex( ctx, EVP_aes_256_gcm(), NULL, NULL, NULL ) ) || 
      ( 1 != EVP_CIPHER_CTX_ctrl( ctx, EVP_CTRL_AEAD_SET_IVLEN, IV_SIZE, NULL ) ) ||
      ( 1 != EVP_EncryptInit_ex( ctx, NULL, NULL, key, iv ) ) )
    {
        status = handleErrors();
    }

    /* Encrypt the plaintext, AAD not included: https://tools.ietf.org/html/rfc5084 */
    if( ( 1 != EVP_EncryptUpdate( ctx, ciphertext, &len, plaintext, plaintext_len ) ) )
    {
        status = handleErrors();
    }

    ciphertext_len = len;

    if( ( 1 != EVP_EncryptFinal_ex( ctx, ciphertext + len, &len ) ) ||
        ( 1 != EVP_CIPHER_CTX_ctrl( ctx, EVP_CTRL_GCM_GET_TAG, GCM_TAG_LEN, tag ) ) )
    {
        status = handleErrors();
    }

    ciphertext_len += len;

    EVP_CIPHER_CTX_free( ctx );

    return ( 0 == status ) ? ciphertext_len : status;
}

//Source: https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
int decryptCBC( byte *ciphertext, int ciphertext_len, byte *key,
             byte *iv, byte *plaintext)
{
    EVP_CIPHER_CTX *ctx;
    int len;
    int plaintext_len;
    int status = 0;

    if( ( !(ctx = EVP_CIPHER_CTX_new() ) ) ||
        ( 1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv ) ) )
    {
        status = handleErrors();
    }

    if( 1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len) )
    {
        status = handleErrors();
    }

    plaintext_len = len;

    if ( 1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len) )
    {
        status = handleErrors();
    }

    plaintext_len += len;

    EVP_CIPHER_CTX_free(ctx);

    return ( 0 == status ) ? plaintext_len : status ;
}

//Source: https://github.com/openssl/openssl/blob/master/demos/evp/aesgcm.c
int decryptGCM( byte *ciphertext, int ciphertext_len, byte *key,
                byte *iv, byte *plaintext, byte *tag )
{
    EVP_CIPHER_CTX *ctx;
    int len;
    int plaintext_len;
    int status = 0;
 
    /* Create and initialise the context */
    if( ( !( ctx = EVP_CIPHER_CTX_new() ) ) ||
      ( 1 != EVP_DecryptInit_ex( ctx, EVP_aes_256_gcm(), NULL, NULL, NULL ) ) || 
      ( 1 != EVP_CIPHER_CTX_ctrl( ctx, EVP_CTRL_GCM_SET_IVLEN, IV_SIZE, NULL ) ) ||
      ( 1 != EVP_DecryptInit_ex( ctx, NULL, NULL, key, iv ) ) )
    {
        status = handleErrors();
    }

    /* Encrypt the plaintext, AAD not included: https://tools.ietf.org/html/rfc5084 */
    if( ( 1 != EVP_DecryptUpdate( ctx, plaintext, &len, ciphertext, ciphertext_len ) ) )
    {
        status = handleErrors();
    }

    plaintext_len = len;

    if( ( 1 != EVP_CIPHER_CTX_ctrl( ctx, EVP_CTRL_GCM_SET_TAG, GCM_TAG_LEN, tag ) ) ||
        ( 0 >= EVP_DecryptFinal_ex( ctx, plaintext + len, &len ) ) )
    {
        status = handleErrors();
    }

    plaintext_len += len;

    EVP_CIPHER_CTX_free( ctx );

    return ( 0 == status ) ? plaintext_len : status;
}

int main( void )
{
    // Generate two ECDH Key Pairs

    EVP_PKEY       *keyA     = NULL; // Alice KeyPair
    EVP_PKEY       *keyB     = NULL; // Bob KeyPair

    genECDHKey( &keyA, NID_secp384r1 );
    genECDHKey( &keyB, NID_secp384r1 );

    BIO *outbio = NULL;

    outbio = BIO_new( BIO_s_file() );
    outbio = BIO_new_fp( stdout, BIO_NOCLOSE );

    if ( NULL == outbio )
    {
        ERR_print_errors_fp( stderr );
        return -1;
    }

    // Show KeyA (Alice)
    fprintf( stdout, "\nAlice's Keys\n\n");
    PEM_write_bio_PUBKEY( outbio, keyA );
    PEM_write_bio_PrivateKey( outbio, keyA, NULL, NULL, 0, 0, NULL);
    fprintf( stdout, "\n");

    fprintf( stdout, "Bob's Keys\n\n");
    // Show KeyB (Bob)
    PEM_write_bio_PUBKEY( outbio, keyB );
    PEM_write_bio_PrivateKey( outbio, keyB, NULL, NULL, 0, 0, NULL);

    size_t         secretLen = 0;
    byte           *secretA  = NULL;
    byte           *secretB  = NULL;

    // Show derived ECDH shared secrets between Alice and Bob - should be the same
    secretA = genECDHSharedSecret( keyA, keyB, &secretLen );

    BIO *b64, *bio;

    b64 = BIO_new( BIO_f_base64 () );
    bio = BIO_new_fp(stdout, BIO_NOCLOSE );
    BIO_set_flags( bio, BIO_FLAGS_BASE64_NO_NL);
    BIO_push( b64, bio );

    fprintf( stdout, "\nAlice genrates a ECDH Shared Secret using Bobs Public Key and her Private key\n" );
    fprintf( stdout, "\nShared Secret A: ");
    BIO_write( b64, secretA, secretLen );
    (void) BIO_flush( b64 );

    byte sessionKeyA[KEY_SIZE]    = { 0 };
    byte nonce[KEY_SIZE]          = { 0 };
    uint counter                  = 1;
    byte plaintext[]              = "Hi Bob, its Alice\n";
    byte iv[IV_SIZE]              = { 0 };
    byte ciphertextCBC[KEY_SIZE]  = { 0 };
    byte ciphertextGCM[KEY_SIZE]  = { 0 };
    byte hmac[KEY_SIZE]           = { 0 };
    byte gcmTag[KEY_SIZE]         = { 0 };
    uint cipherLenCBC             = 0;
    uint cipherLenGCM             = 0;
    uint hmacLen                  = 0;

    // Encrypt what Alice sends to Bob
    if ( ( 1 != RAND_bytes( nonce, sizeof( nonce) ) ) ||
         ( 1 != genSessionKey( sessionKeyA, secretA, secretLen, nonce, &counter, KEY_SIZE ) ) ||
         ( 1 != RAND_bytes( iv, sizeof( iv ) ) ) ||
         ( -1 == ( cipherLenCBC = encryptCBC( plaintext, sizeof( plaintext ), 
                                              sessionKeyA, iv, ciphertextCBC ) ) ) ||
         ( -1 == ( cipherLenGCM = encryptGCM( plaintext, sizeof( plaintext ), 
                                              sessionKeyA, iv, ciphertextGCM, gcmTag ) ) ) ||
         ( NULL == HMAC( EVP_sha256(), sessionKeyA, sizeof( sessionKeyA ), ciphertextCBC, 
                         cipherLenCBC, hmac, &hmacLen ) ) )
    {
        ERR_print_errors_fp( stderr );
        return -1;
    }
    
    fprintf( stdout, "    Session Key: ");
    BIO_write( b64, sessionKeyA, sizeof( sessionKeyA ) );
    (void) BIO_flush( b64 );
    fprintf( stdout, "      Plaintext: %s", plaintext );
    fprintf( stdout, "CBC Cipher Text: ");
    BIO_write( b64, ciphertextCBC, cipherLenCBC );
    (void) BIO_flush( b64 );
    fprintf( stdout, "GCM Cipher Text: ");
    BIO_write( b64, ciphertextGCM, cipherLenGCM );
    (void) BIO_flush( b64 );
    fprintf( stdout, "             IV: ");
    BIO_write( b64, iv, sizeof( iv ) );
    (void) BIO_flush( b64 );
    fprintf( stdout, "          Nonce: ");
    BIO_write( b64, nonce, sizeof( nonce ) );
    (void) BIO_flush( b64 );
    fprintf( stdout, "           HMAC: ");
    BIO_write( b64, hmac, hmacLen );
    (void) BIO_flush( b64 );
    fprintf( stdout, "        GCM Tag: ");
    BIO_write( b64, gcmTag, GCM_TAG_LEN );
    (void) BIO_flush( b64 );
    fprintf( stdout, "\n\nMessage is Sent from Alice to Bob.........................\n");

    byte decypt_pt[KEY_SIZE]    = { 0 };
    byte decypt_ptGCM[KEY_SIZE] = { 0 };
    byte sessionKeyB[KEY_SIZE]  = { 0 };
    byte hmacVerify[KEY_SIZE]   = { 0 };
    uint hmacVerifyLen          = 0;

    secretB = genECDHSharedSecret( keyB, keyA, &secretLen );

    fprintf( stdout, "\nBob genrates the same ECDH Shared Secret using Alices Public Key and his Private key\n\n" );
    fprintf( stdout, "Shared Secret B: ");
    BIO_write( b64, secretB, secretLen );
    (void) BIO_flush( b64 );

    // Decrypt what Bob gets from Alice
    if ( ( 1 != genSessionKey( sessionKeyB, secretB, secretLen, nonce, &counter, KEY_SIZE ) ) ||
         ( NULL == HMAC( EVP_sha256(), sessionKeyB, sizeof( sessionKeyB ),
                         ciphertextCBC, cipherLenCBC, hmacVerify, &hmacVerifyLen) ) ||
         ( -1 == decryptCBC( ciphertextCBC, cipherLenCBC, sessionKeyB, iv, decypt_pt ) ) ||
         ( -1 == decryptGCM( ciphertextGCM, cipherLenGCM, sessionKeyB, iv, decypt_ptGCM, gcmTag ) ) ) 
    {
        ERR_print_errors_fp( stderr );
        return -1;
    }

    fprintf( stdout, "    Session Key: ");
    BIO_write( b64, sessionKeyB, sizeof( sessionKeyB ) );
    (void) BIO_flush( b64 );
    fprintf( stdout, " CBC Plain Text: %s", decypt_pt );
    fprintf( stdout, " GCM Plain Text: %s", decypt_ptGCM );
    fprintf( stdout, "             IV: ");
    BIO_write( b64, iv, sizeof( iv ) );
    (void) BIO_flush( b64 );
    fprintf( stdout, "          Nonce: ");
    BIO_write( b64, nonce, sizeof( nonce ) );
    (void) BIO_flush( b64 );
    fprintf( stdout, "           HMAC: ");
    BIO_write( b64, hmacVerify, hmacVerifyLen );
    (void) (void) BIO_flush( b64 );
    fprintf( stdout, "        GCM Tag: ");
    BIO_write( b64, gcmTag, GCM_TAG_LEN );
    (void) BIO_flush( b64 );
    fprintf( stdout, "\n");

    free( secretA );
    free( secretB );
    EVP_PKEY_free( keyA );
    EVP_PKEY_free( keyB );
    BIO_free_all( b64 );
    BIO_free_all( outbio );

    return 0;
}
