# README #

This source code is for demo purposes only and is meant to be used to model SSL Client/Server interactions

### Getting set up ###

Use the local Makefile to do the work for use. Inside the ./crypt_demo dir:

	/crypt_demo$ make openssl
	/crypt_demo$ make all

This will also generate the required certificates. Note that this requires a native installation of openssl on your system.

### Running the software ###

in the "bin" directory run the server client using:

	/crypt_demo/bin$ ./sslserver ../certs/server.pem ../certs/server.key ../certs/ca.pem
	/crypt_demo/bin$ ./sslclient ../certs/client.pem ../certs/client.key ../certs/ca.pem

The default setting is for client cert based authentication. If you dont
want this set the enClientCertAuth variable to 0 in both source files.

To run the Basic Crypt example from the "bin" directory:

	/crypt_demo/bin$ ./cryptexample

This shows a sample set of keys being used for and AES-CBC-HMAC exchange,
the session key is generated using an HKDF

### Building in Docker ###

If you dont have the required environment you can build everything in a docker container using the Dockerfile provided.

	/crypt_demo$ docker build . -t="crypt_demo"
	/crypt_demo$ docker run -it crypt_demo bash

This will bring you to a root bash session inside the container where you can run the code

Hopefully this is useful, have fun.
