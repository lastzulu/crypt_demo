#!/bin/bash

echo Making Keys...
openssl genrsa -out ca.key 2048
openssl genrsa -out server.key 2048
openssl genrsa -out client.key 2048

openssl genrsa -out ca2.key 2048
openssl genrsa -out server2.key 2048
openssl genrsa -out client2.key 2048

#echo Creating Self Signed CAs
openssl req -x509 -nodes -new -key ca.key -config ca.conf -extensions v3_ca -out ca.pem -days 365
openssl req -x509 -nodes -new -key ca2.key -config ca.conf -extensions v3_ca -out ca2.pem -days 365

echo Creating Server Certificate
openssl req -new -key server.key -config server.conf -out server.csr
openssl x509 -req -in server.csr -CA ca.pem -CAkey ca.key -CAcreateserial -days 365 -sha256 -extfile server.conf -extensions v3_server -out server.pem

echo Createing Client Certificate
openssl req -new -key client.key -config client.conf -out client.csr
openssl x509 -req -in client.csr -CA ca.pem -CAkey ca.key -CAcreateserial -days 365 -sha256 -extfile client.conf -extensions v3_client  -out client.pem

echo Creating Server Certificate 2
openssl req -new -key server2.key -config server.conf -out server2.csr
openssl x509 -req -in server2.csr -CA ca2.pem -CAkey ca2.key -CAcreateserial -days 365 -sha256 -extfile server.conf -extensions v3_server -out server2.pem

echo Createing Client Certificate 2
openssl req -new -key client2.key -config client.conf -out client2.csr
openssl x509 -req -in client2.csr -CA ca2.pem -CAkey ca2.key -CAcreateserial -days 365 -sha256 -extfile client.conf -extensions v3_client  -out client2.pem

echo Cleanup
rm *.srl
rm *.csr
