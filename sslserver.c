#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>
#include <pthread.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509.h>

#define MAX_BUFF 1024
#define MAX_CHAIN_VERIFY_DEPTH   3

static char*              sslServerCert     = NULL;
static char*              sslServerKey      = NULL;
static char*              sslCACert         = NULL;

static const unsigned int enClientCertAuth  = 1;
static const char*        cipherList        = "TLS_AES_256_GCM_SHA384:" \
                                              "TLS_CHACHA20_POLY1305_SHA256:" \
                                              "TLS_AES_128_GCM_SHA256:" \
                                              "ECDHE+AESGCM:ECDHE+AES256"; //TLS1_3,TLS1_2
static const long         sslOptions        = SSL_OP_NO_TLSv1_1 |
                                              SSL_OP_NO_TLSv1 |
                                              SSL_OP_NO_SSLv3 |
                                              SSL_OP_NO_SSLv2;
static const unsigned int serverPort        = 8443;
static SSL_CTX            *sslServerCtx     = NULL;
static int                serverSocketfd    = 0;
static const char*        clientEmail       = "yname@test.com";

static void _shutDown( int sig )
{
    close( serverSocketfd );
    SSL_CTX_free( sslServerCtx );

    fprintf( stdout, "\nExiting\n" );
}

static unsigned int _peerVerify( SSL *ssl )
{
    unsigned int result = 0; // default fails
    X509 *peerCert      = SSL_get_peer_certificate( ssl );

    if( NULL != peerCert )
    {
        long verifyResult = SSL_get_verify_result( ssl );

        switch( verifyResult )
        {
           case X509_V_OK:
               fprintf( stderr, "Client certificate Verify Success\n" );
               result = 1; // peer validates
               break;
           default:
               fprintf( stderr, "Client certificate Verify Failed\n" );
               break;
        }

        X509_free( peerCert );
    }
    else
    {
        fprintf( stderr, "There is no peer certificate\n" );
    }

    return result;
}

static void _processInputBuffer( SSL *ssl )
{
    // Process data inside TLS
    char buffer[MAX_BUFF] = { 0 };
    int bytesRead         = 0;

    if ( 0 < ( bytesRead = SSL_read( ssl, buffer, sizeof( buffer ) -1 ) ) )
    {
       buffer[bytesRead] = '\0';
       fprintf( stdout, "Received msg: %s\n", buffer );
       fflush( stdout );
    }
}

static void* _connHandler( void *socket_desc ) 
{
    int     clientsocketfd        = *(int*) socket_desc;
    int     status                = 1; // SUCCESS
    SSL     *serverssl            = NULL;
    X509_VERIFY_PARAM  *param     = NULL;

    if ( ( NULL != ( serverssl = SSL_new( sslServerCtx ) ) ) &&
         ( 1 == SSL_set_cipher_list( serverssl, cipherList ) ) )
    {
        if ( enClientCertAuth )
        {
            if ( ( NULL == ( param = SSL_get0_param( serverssl ) ) ) ||
                 ( 1 != X509_VERIFY_PARAM_set1_email( param, clientEmail, // verify email
                                                      strlen( clientEmail ) ) ) )
            {
                ERR_print_errors_fp( stderr );
                status = 0; //something broke, quit.....
            }
        }
        if ( ( status ) &&
           ( 1 == SSL_set_fd( serverssl, clientsocketfd ) ) &&
           ( 1 == SSL_accept( serverssl ) ) )
        {
            if ( enClientCertAuth )
            {
                if ( _peerVerify( serverssl ) )
                {
                    _processInputBuffer( serverssl );
                }
            }
            else
            {
                _processInputBuffer( serverssl );
            }
        }
        else
        {
            ERR_print_errors_fp( stderr );
        }

        SSL_shutdown( serverssl );
        SSL_free( serverssl );
    }
    else
    {
        ERR_print_errors_fp( stderr );
    }

    close( clientsocketfd );
    return NULL;
}

int main( int argc, char **argv )
{
    if ( 4 != argc )
    {
        fprintf( stderr, "expecting <server.pem> <server.key> <ca.pem>\n" );
        return -1;
    }
    else
    {
        sslServerCert = argv[1];
        sslServerKey  = argv[2];
        sslCACert     = argv[3];
    }

    SSL_library_init();
    SSL_load_error_strings();
    const SSL_METHOD *serverMethod  = TLS_server_method();
    sslServerCtx                    = SSL_CTX_new( serverMethod );

    if ( ( NULL == sslServerCtx ) ||
         ( 1 != SSL_CTX_use_certificate_file( sslServerCtx, sslServerCert, SSL_FILETYPE_PEM ) ) ||
         ( 1 != SSL_CTX_use_PrivateKey_file( sslServerCtx, sslServerKey, SSL_FILETYPE_PEM) ) ||
         ( 1 != SSL_CTX_check_private_key( sslServerCtx) ) ||
         ( 0 == SSL_CTX_set_max_proto_version( sslServerCtx, TLS1_3_VERSION ) ) ||
         ( 0 == SSL_CTX_set_cipher_list( sslServerCtx, cipherList ) ) ||
         ( sslOptions == SSL_CTX_set_options( sslServerCtx, sslOptions ) ) )
    {
        ERR_print_errors_fp( stderr );
        fprintf( stdout, "Did you forget to make certs?\n");
        return -1;
    }

    if ( enClientCertAuth )
    {
        // Extra steps for setting client cert auth
        if ( 0 == SSL_CTX_load_verify_locations( sslServerCtx, sslCACert, NULL ) )
        {
            ERR_print_errors_fp( stderr );
            return -1;
        }

        SSL_CTX_set_verify( sslServerCtx, SSL_VERIFY_PEER, NULL );
        SSL_CTX_set_verify_depth( sslServerCtx, MAX_CHAIN_VERIFY_DEPTH );
    }

    struct sockaddr_in serveraddr = { 0 };
    serveraddr.sin_family         = AF_INET;
    serveraddr.sin_addr.s_addr    = INADDR_ANY;          // server address
    serveraddr.sin_port           = htons( serverPort ); // server port

    fprintf( stdout, "Listening on port: %d\n", serverPort );

    if ( ( -1 == ( serverSocketfd = socket( AF_INET, SOCK_STREAM, 0) ) ) ||
         ( -1 == bind( serverSocketfd, (struct sockaddr *) &serveraddr,
                       sizeof( struct sockaddr_in ) ) ) ||
         ( -1 == listen( serverSocketfd, SOMAXCONN ) ) )
    {
        fprintf( stderr, "Socket initialization ERRNO: %d\n", errno );
        return -1;
    }

    signal( SIGINT, _shutDown );

    int clientsocketfd  = 0;
    pthread_t thread_id;

    while ( -1 != ( clientsocketfd = accept( serverSocketfd, NULL, 0 ) ) )
    {
        if( 0 > pthread_create( &thread_id, NULL, _connHandler, (void*) &clientsocketfd ) )
        {
            fprintf( stderr, "could not create thread\n" );
        }
        else
        {
            pthread_detach( thread_id );
        }
    }

    return 0;
}
