CC     = gcc
SSLDIR = ./openssl
CFLAGS = -I$(SSLDIR)/include -g -Wall
SLIBS  = $(SSLDIR)/libssl.a $(SSLDIR)/libcrypto.a 
DLIBS  = -lpthread -ldl
BINDIR = bin/
CERTS  = certs/

%.o: %.c 
	$(CC) -c -o $@ $< $(CFLAGS)

sslclient: sslclient.o
	$(CC) -o $(BINDIR)$@ $^ $(SLIBS) $(DLIBS)

sslserver: sslserver.o
	$(CC) -o $(BINDIR)$@ $^ $(SLIBS) $(DLIBS)

cryptexample: cryptexample.o
	$(CC) -o $(BINDIR)$@ $^ $(SLIBS) $(DLIBS)

all: sslclient sslserver cryptexample certs

.PHONY: clean
.PHONY: certs

clean:
	rm -f *.o 
	rm -f $(BINDIR)sslclient 
	rm -f $(BINDIR)sslserver
	rm -f $(BINDIR)cryptexample
	rm -f $(CERTS)*.pem $(CERTS)*.csr
	rm -f $(CERTS)*.key $(CERTS)*.srl

openssl:
	git clone --depth 1 https://github.com/openssl/openssl; \
	cd openssl; \
	./config --openssldir=./; \
	make all

certs:
	cd $(CERTS); \
	./makecerts.sh

