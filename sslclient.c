#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509_vfy.h>

#define MAX_BUFF 1024
#define MAX_CHAIN_VERIFY_DEPTH   3

static char*              sslClientCert     = NULL;
static char*              sslClientKey      = NULL;
static char*              sslCACert         = NULL;

static const unsigned int enClientCertAuth = 1;
static const char*        cipherList       = TLS_DEFAULT_CIPHERSUITES;
static const long         sslOptions       = SSL_OP_NO_TLSv1 |
                                             SSL_OP_NO_SSLv3 |
                                             SSL_OP_NO_SSLv2;
static const unsigned int serverPort       = 8443;
static const char*        serverAddress    = "127.0.0.1";
static const char*        serverHostName   = "test.com";

static SSL_CTX            *sslClientCtx  = NULL;
static int                clientSocketfd = 0;

static unsigned int peerVerify( SSL *ssl )
{
    unsigned int       result    = 0; // default fails
    X509               *peerCert = SSL_get_peer_certificate( ssl );

    if( NULL != peerCert )
    {
        long verifyResult = SSL_get_verify_result( ssl );

        switch( verifyResult )
        {
            case X509_V_OK:
                fprintf( stderr, "Server Certificate Verify Success\n" );
                result = 1;
                break;
            default:
                fprintf( stderr, "Server Certificate Verify Failed\n" );
                break;
        }

        X509_free( peerCert );
    }
    else
    {
        fprintf( stderr, "There is no peer certificate\n" );
    }

    return result;
}

static void processOutputBuffer( SSL *ssl )
{
    // Process data inside TLS
    char buffer[MAX_BUFF] = { 0 };
    int  bytesWritten                     = 0;

    if ( NULL != fgets( buffer, sizeof( buffer ), stdin ) )
    {
        if ( 0 < ( bytesWritten = SSL_write( ssl, buffer, strlen( buffer ) ) ) )
        {
           fprintf( stdout, "\nSent msg: %s\n", buffer );
        }
    }
}

int main( int argc, char **argv )
{
    if ( 4 != argc )
    {
        fprintf( stderr, "expecting <client.pem> <client.key> <ca.pem>\n" );
        return -1;
    }
    else
    {
        sslClientCert = argv[1];
        sslClientKey  = argv[2];
        sslCACert     = argv[3];
    }

    SSL_library_init();
    SSL_load_error_strings();
    const SSL_METHOD *clientMethod  = TLS_client_method();
    sslClientCtx                    = SSL_CTX_new( clientMethod );

    if ( ( NULL == sslClientCtx ) ||
         ( 0 == SSL_CTX_load_verify_locations( sslClientCtx, sslCACert, NULL ) ) ||
         ( 0 == SSL_CTX_set_max_proto_version( sslClientCtx, TLS1_3_VERSION ) ) ||
         ( 0 == SSL_CTX_set_cipher_list( sslClientCtx, cipherList ) ) ||
         ( sslOptions == SSL_CTX_set_options( sslClientCtx, sslOptions ) ) )
    {
        ERR_print_errors_fp( stderr );
        return -1;
    }

    SSL_CTX_set_verify( sslClientCtx, SSL_VERIFY_PEER, NULL );
    SSL_CTX_set_verify_depth( sslClientCtx, 1 );

    if ( enClientCertAuth )
    {
        if ( ( NULL == sslClientCtx ) ||
           ( 1 != SSL_CTX_use_certificate_file( sslClientCtx, sslClientCert, SSL_FILETYPE_PEM ) ) ||
           ( 1 != SSL_CTX_use_PrivateKey_file( sslClientCtx, sslClientKey, SSL_FILETYPE_PEM) ) ||
           ( 1 != SSL_CTX_check_private_key( sslClientCtx) ) )
        {
            ERR_print_errors_fp( stderr );
            fprintf( stdout, "Did you forget to make certs?\n");
            return -1;
        }
    }

    struct sockaddr_in serveraddr = { 0 };
    serveraddr.sin_family         = AF_INET;
    serveraddr.sin_addr.s_addr    = inet_addr( serverAddress ); // server address
    serveraddr.sin_port           = htons( serverPort );        // server port

    if ( ( -1 == ( clientSocketfd = socket( AF_INET, SOCK_STREAM, 0) ) ) ||
         ( -1 == connect( clientSocketfd, (struct sockaddr *) &serveraddr,
                          sizeof( struct sockaddr_in ) ) ) )
    {
       fprintf( stderr, "Socket initialization ERRNO: %d\n", errno );
       return -1;
    }

    SSL *clientssl               = NULL;
    X509_VERIFY_PARAM  *param    = NULL;

    if ( ( NULL != ( clientssl = SSL_new( sslClientCtx ) ) ) &&
         ( NULL != ( param = SSL_get0_param( clientssl ) ) ) &&
         ( 1 == X509_VERIFY_PARAM_set1_host( param, serverHostName, // verify hostname
                                             strlen( serverHostName ) ) ) &&
         ( 1 == SSL_set_fd( clientssl, clientSocketfd ) ) &&
         ( 1 == SSL_connect( clientssl ) ) )
    {
        if ( peerVerify( clientssl ) )
        {
            processOutputBuffer( clientssl );
        }

        SSL_shutdown( clientssl );
        SSL_free( clientssl );
    }
    else
    {
        ERR_print_errors_fp( stderr );
    }

	close( clientSocketfd );
	SSL_CTX_free( sslClientCtx );
	return 0;	
}
